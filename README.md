# Project - "Where am I"

Retrieve user location with external api.

## Demo 

https://where-am-i-game.netlify.app

## Tools

- Purely html, css and js
- ipinfo.io api call